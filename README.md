# README #

### What is this repository for? ###

This repo contains Linux debug API that sends TimeToPic compatible traces to host. 
Code can act either server mode when logging application connects to target or client mode when target connects to logger. 

Library is written using C++11 STL and uses sockets for sending data. 
Developer can then log code behavior using functions such as:

TimeToPicLog.h

- logText: Log text. 
- logEventStart & logEventStop: Log events that have start and stop. WIll be visualized as bar on timeline. 
- logValue: Logs value. Will be visualized and graph on timeline. 
- logState: Logs state machine. Will be visualized as bar on timeline where state is visualized to user. 
- logMessage: Log messages. Currently not visualized on TimeToPic but is visualized on TimeToPic Grapher. 

TimeToPicLinuxLoggerAPI.h

If TTPLOGGING is defined:

- TTPLOG_EVENT
- TTPLOG_FUNCTIONSCOPE: Automatic scope tracing. Will use _FUNCTION__ as event name and is visualized on timeline. 
- TTPLOG_MESSAGE
- TTPLOG_POSTCODE 
- TTPLOG_STRING
- TTPLOG_STATE
- TTPLOG_VALUE 

when TTPLOGGING is not defined, macros above will be empty.

Read more about TimeToPic from http://timetopic.herwoodtechnologies.com.



### How do I get set up? ###

##### Option 1 by including files directly to project
User should add **TimeToPicLog.cpp*, TimeToPicLinuxLoggerAPI.h and TimeToPicLog.h** to target application project and compile.
File TimeToPicLinuxLoggerClient.cpp contains simple examples how to use. Read more about TimeToPic from http://timetopic.herwoodtechnologies.com.

##### Option 2 by including subproject to Cmake project. See minimal example below of root level CmakeLists.txt

```
cmake_minimum_required (VERSION 3.4)
#Generates build for static library that can be linked to target. 
project (RaspTestProject)
add_subdirectory (timetopic-logger-linux-logger-api)
add_subdirectory (poco)
add_subdirectory (RaspApp)
```

#### Configuration

##### Case 1: Target connecting to logger (client mode) 

User code should do following initialization somewhere
```
auto &l = TimeToPicLog::instance();
l.setClientMode(); // target will connect Logger (IP:127.0.0.1 port 5000)
```
and once there is connection, all traces sent should appear to logger. 
IP address can be changed. See
```
	bool setClientMode(string destinationTcpAddress = "127.0.0.1",
			int portNumber = defaultTimeToPicTcpRemoteConnectionPort);
```

##### Case 2: logger connecting to target (server mode) 
User code should do following initialization somewhere
```
auto &l = TimeToPicLog::instance();
l.setServerMode(); // target will connect Logger (IP:127.0.0.1 port 5000)
```
Default configuration starts client on server mode and listens TCP port 5000. TimeToPic Logger application should be configured to client connection mode. 

#### Dependencies
STL standard libraries, phtread and socket library are used. User should add phtread library to linking parameters. 