//============================================================================
// Name        : TimeToPicLinuxLoggerClient.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
//

#define TTPLOGGING
#include "../TimeToPicLinuxLoggerAPI.h"

using namespace std;

int main() {
	cout << "TimeToPic logger tester" << endl;

	try {
		auto &l = TimeToPicLog::instance();
		//l.waitForConnection(10); // Wait that we have connection before proceeding.
		l.setClientMode(); // target will connect Logger (IP:127.0.0.1 port 5000)
		//l.setServerMode(); // target acts as server and logger should connect this.
		TTPLOG_POSTCODE("Test started");
		TTPLOG_FUNCTIONSCOPE; // example macro about scope logging.

		TTPLOG_POSTCODE("Test running");
		while (1) {
			scopeLog("while");
			std::this_thread::sleep_for(std::chrono::seconds(1));
			l.logText(string("Message from main thread"));
		}

	} catch (std::exception& e) {
		/* Initialization failed */
		cout << "TimeToPic logger tester failed" << endl;
		int exitVar = 0;
		cin >> exitVar;
	}
	return 0;
}
