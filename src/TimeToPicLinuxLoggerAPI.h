#ifndef TIMETOPICLOGAPI_H_
#define TIMETOPICLOGAPI_H_

#include "TimeToPicLog.h"

class scopeLog {
public:
  scopeLog(std::string scope = "unknown")
      : mScope(scope), mLogger(TimeToPicLog::instance()) {
    mLogger.logEventStart(mScope);
  }
  ~scopeLog() { mLogger.logEventStop(mScope); }

private:
  std::string mScope;
  TimeToPicLog &mLogger;
};

/* #define TTPLOGGING somewhere before header if you want use these */

#ifdef TTPLOGGING

#define TTPLOG_EVENT(channel, state)                                           \
  TimeToPicLog::instance().logEvent(channel, state)

#define TTPLOG_VALUE(channel, value)                                           \
  TimeToPicLog::instance().logValue(channel, value)

#define TTPLOG_STATE(channel, state)                                           \
  TimeToPicLog::instance().logState(channel, state)

#define TTPLOG_STRING(str) TimeToPicLog::instance().logText(str)

#define TTPLOG_POSTCODE(str)                                                   \
  TimeToPicLog::instance().logText(string("postcode ") + str)

/* Macro for reporting messaging between entitities */
#define TTPLOG_MESSAGE(source, destination, messagename)                       \
  TimeToPicLog::instance().logMessage(source, destination, messagename)

#define TTPLOG_FUNCTIONSCOPE scopeLog timeToPicLoggerScopeObj(__FUNCTION__)
#else
#define TTPLOG_FUNCTIONSCOPE
#define TTPLOG_EVENT
#define TTPLOG_VALUE
#define TTPLOG_STATE
#define TTPLOG_STRING
#define TTPLOG_POSTCODE
#define TTPLOG_MESSAGE
#endif

#endif // TIMETOPICLOGAPI_H_
