/*
 * TimeToPicLog.cpp
 *
 *  Created on: Jan 23, 2019
 *      Author: erkki
 */

#include <cstring>
#include <iostream>
#include <memory>
#include <thread>
//
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <sys/socket.h>
#include <unistd.h>
//
#include "TimeToPicLog.h"

static TimeToPicLog *gInstance = nullptr;

TimeToPicLog::TimeToPicLog() {

    bool initOk = true;

    try {
        /* Init stuff here */

    } catch (std::exception &) {
        initOk = false;
    }

    if (initOk == false) {
        throw LoggerInitFailException();
    }
}

TimeToPicLog::~TimeToPicLog() { stopSenderThreadProc(); }

TimeToPicLog &TimeToPicLog::instance() {
    if (gInstance == nullptr) {
        gInstance = new TimeToPicLog();
    }
    return *gInstance;
}

void TimeToPicLog::stopInstance() {
    if (gInstance) {
        delete gInstance;
        gInstance = nullptr;
    }
}

void TimeToPicLog::logText(const string &logStr) {
    string logText =
        formatRelativeTimeStampSec(getRelativeTimeStamp()) + ";" + logStr + "\n";
    sendText(logText);
}

void TimeToPicLog::sendText(const string &text) {

    /* Lock access to list */
    if (mConnectionIsSetUp) {
        std::lock_guard<std::mutex> lock(mLogLineTxListMutex);

        if (mLogLineTxList.size() < maxLogLinesInQueue) {
            mLogLineTxList.push_back(text);
        } else if (mLogLineTxList.size() == maxLogLinesInQueue) {
            mLogLineTxList.push_back(
                "error: Exceeded TX queue size. Dropping data\n");
        } else {
            /* Drop data */
        }
    }
}

TimeToPicLog::relativeTimeStampType TimeToPicLog::getRelativeTimeStamp() {
    auto end = std::chrono::system_clock::now();
    relativeTimeStampType diff = end - mInstanceStartTime;
    return diff;
}

bool TimeToPicLog::setServerMode() {

    bool ret = false;
    stopSenderThreadProc();
    mConnectionIsSetUp = false;

    mConnectionMode = tcpServer;

    mSenderThread =
        std::make_shared<std::thread>([this] { senderThreadProc("", 0); });
    if (mSenderThread) {
        ret = true;
    }
    return ret;
}

bool TimeToPicLog::setClientMode(string destinationTcpAddress, int portNumber) {
    bool ret = true;

    stopSenderThreadProc();
    mConnectionIsSetUp = false;
    mConnectionMode = tcpClient;

    mSenderThread =
        std::make_shared<std::thread>([this, destinationTcpAddress, portNumber] {
            senderThreadProc(destinationTcpAddress, portNumber);
        });
    if (mSenderThread) {
        ret = true;
    }
    return ret;
}

string
TimeToPicLog::formatRelativeTimeStampSec(relativeTimeStampType timeStamp) {

    std::ostringstream formatter;
    formatter << std::fixed << timeStamp.count();
    std::string str = formatter.str();
    return str;
}

void TimeToPicLog::senderThreadProc(string destinationTcpAddress,
                                    int portNumber) {

    while (mSenderThreadAbortRequested == false) {

        if (mConnectionIsSetUp == false) {
            switch (mConnectionMode) {
            case tcpClient:
                setupClientConnection(destinationTcpAddress, portNumber);
                break;
            case tcpServer:
                setupServerListener();
                break;
            }
        }

        while (mSenderThreadAbortRequested == false && mConnectionIsSetUp == true) {

            if (mLogLineTxList.empty() == false) {

                /* Lock access to list */
                std::lock_guard<std::mutex> lock(mLogLineTxListMutex);

                while (mLogLineTxList.empty() == false) {

                    auto l = mLogLineTxList.front();
                    mLogLineTxList.pop_front();

                    /* Later this could be optimized to send bigger chunks */
                    mConnectionIsSetUp = sendToActiveConnection(l);
                    if (mConnectionIsSetUp == false) {
                        /* Remote disconnected. Add indicator and exit loop */
                        mLogLineTxList.push_back("error: Remote disconnected\n");
                        break;
                    }
                }
            } else {
                /* Now sleep a short perioid */
                const int defaultSleepTimeMs = 100;
                std::this_thread::sleep_for(
                    std::chrono::milliseconds(defaultSleepTimeMs));
            }
        }
    }
}

void TimeToPicLog::stopSenderThreadProc() {
    if (mSenderThread) {

        /* Set exit flag */
        mSenderThreadAbortRequested = true;

        /* Wait that thread exists */
        mSenderThread->join();

        mSenderThreadAbortRequested = false;
        mSenderThread = nullptr;
    }
}

bool TimeToPicLog::sendToActiveConnection(const string &text) {
    bool ret = false;
    if (text.empty() == false) {
        if (mConnectionIsSetUp == true) {
            if (send(mNewsockfd, text.c_str(), text.length(), MSG_NOSIGNAL) == -1) {
                /* Cannot send any longer */
                closeActiveConnection();
                ret = false;
            } else {
                ret = true;
            }
        }
    }
    return ret;
}

bool TimeToPicLog::setupServerListener() {

    bool ret = false;

    mSockfd = socket(AF_INET, SOCK_STREAM, 0);
    int enable = 1;
    if (setsockopt(mSockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) >=
        0) {

        struct timeval tv = {};
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        setsockopt(mSockfd, SOL_SOCKET, SO_RCVTIMEO,
                   reinterpret_cast<const char *>(&tv), sizeof tv);

        std::memset(&mServerAddress, 0, sizeof(mServerAddress));
        mServerAddress.sin_family = AF_INET;
        mServerAddress.sin_addr.s_addr = htonl(INADDR_ANY);
        mServerAddress.sin_port = htons(defaultTimeToPicTcpRemoteConnectionPort);
        if (bind(mSockfd, reinterpret_cast<struct sockaddr *>(&mServerAddress),
                 sizeof(mServerAddress)) == 0) {
            const int maxConnections = 1;
            listen(mSockfd, maxConnections);

            socklen_t sosize = sizeof(mClientAddress);
            mNewsockfd =
                accept(mSockfd, reinterpret_cast<struct sockaddr *>(&mClientAddress),
                       &sosize);
            if (mNewsockfd != invalidHandleValue) {
                string str = inet_ntoa(mClientAddress.sin_addr);
                mConnectionIsSetUp = true;
                logText("Connected to logger target");
                ret = true;
            } else {
                closeActiveConnection();
            }
        } else {
            auto errorNumber = errno;

            if (errorNumber == EACCES) {
                /* Does not have access */
            } else if (errorNumber == EADDRINUSE) {
                /* Address is in use */
            }
        }
    }

    return ret;
}

bool TimeToPicLog::setupClientConnection(string address, int portNumber) {
    bool ret = false;

    struct hostent *he = nullptr;

    if ((he = gethostbyname(address.c_str())) !=
        nullptr) { /* get the host info */
        if ((mNewsockfd = socket(AF_INET, SOCK_STREAM, 0)) !=
            -1) { // 0 = any protocol

            struct timeval tv = {};
            tv.tv_sec = 1;
            tv.tv_usec = 0;
            setsockopt(mNewsockfd, SOL_SOCKET, SO_RCVTIMEO,
                       reinterpret_cast<const char *>(&tv), sizeof tv);

            mClientAddress = {};
            mClientAddress.sin_family = AF_INET; /* host byte order */
            mClientAddress.sin_port = htons(
                static_cast<in_port_t>(portNumber)); /* short, network byte order */
            mClientAddress.sin_addr.s_addr = inet_addr(address.c_str());

            int connectionResult = connect(
                mNewsockfd, reinterpret_cast<struct sockaddr *>(&mClientAddress),
                sizeof(mClientAddress));

            int err = errno;

            if (connectionResult == 0) {

                fd_set fdset = {};
                FD_ZERO(&fdset);
                FD_SET(mNewsockfd, &fdset);
                struct timeval tv = {};
                tv.tv_sec = 1; /* 1 second timeout */
                tv.tv_usec = 0;

                int rv = select(mNewsockfd + 1, nullptr, &fdset, nullptr, &tv);
                if (rv == 0) {

                } else if (rv == 1) {
                    int so_error = {};
                    socklen_t len = sizeof so_error;

                    getsockopt(mNewsockfd, SOL_SOCKET, SO_ERROR, &so_error, &len);

                    if (so_error == 0) {
                        // socket is open
                        string str = inet_ntoa(mClientAddress.sin_addr);
                        mConnectionIsSetUp = true;
                        logText("Connected to logger target");
                        ret = true;
                    } else {
                        // opening failed
                        closeActiveConnection();
                    }
                }
            } else if (connectionResult == -1 && err == ECONNREFUSED) {
                /* If connecting to localhost, put some delay if connection fails */
                const int defaultSleepTimeMs = 100;
                usleep(defaultSleepTimeMs * 1000);
                closeActiveConnection();

            } else {
                /* Connection failed now */
                closeActiveConnection();
            }
        }
    }

    return ret;
}

void TimeToPicLog::waitForConnection(int maxWaitSec) {

    int waitTimeMsec = 0;
    while (mConnectionIsSetUp == false && waitTimeMsec < maxWaitSec * 1000) {
        const int defaultSleepTimeMs = 100;
        std::this_thread::sleep_for(std::chrono::milliseconds(defaultSleepTimeMs));
        waitTimeMsec += defaultSleepTimeMs;
    }
}

bool TimeToPicLog::closeActiveConnection() {
    bool ret = false;
    if ( mSockfd!=invalidHandleValue) {
        shutdown(mSockfd, SHUT_RDWR);
        close(mSockfd);
        mSockfd = invalidHandleValue;
    }
    if ( mNewsockfd!=invalidHandleValue) {
        shutdown(mNewsockfd, SHUT_RDWR);
        close(mNewsockfd);
        mNewsockfd = invalidHandleValue;
    }

    mConnectionIsSetUp = false;
    return ret;
}
