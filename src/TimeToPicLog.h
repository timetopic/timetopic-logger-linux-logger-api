/*
 * TimeToPicLog.h
 *
 *  Created on: Jan 23, 2019
 *      Author: erkki
 */
#ifndef TIMETOPICLOG_H_
#define TIMETOPICLOG_H_

#include <chrono>
#include <exception>
#include <iomanip>
#include <list>
#include <memory>
#include <mutex>
#include <sstream>
#include <string>
#include <thread>

// headers for socket stuff
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

using namespace std;

struct LoggerInitFailException : public exception {
  const char *what() const noexcept {
    return "TimeToPic logger initialization failed";
  }
};

struct LoggerInternalsFailException : public exception {
  const char *what() const noexcept { return "LoggerInternalsFailException"; }
};

class TimeToPicLog {
public:
  /* You need add 'pthread' to your libraries */

  /* Max amount of data to be added to queue */
  static const int maxLogLinesInQueue = 100000;
  static const int defaultTimeToPicTcpRemoteConnectionPort = 5000;

  TimeToPicLog(); // Throws LoggerInitFailException if init fails.
                  // Sets sigaction(SIGPIPE, &(struct sigaction){SIG_IGN},
                  // NULL);
  virtual ~TimeToPicLog();

  TimeToPicLog(TimeToPicLog const &) = delete;
  TimeToPicLog &operator=(TimeToPicLog const &) = delete;

  /* Create singleton item for logging */
  static TimeToPicLog &
  instance(); // Throws LoggerInitFailException if init fails

  /* Deletion of singleton */
  static void stopInstance();

  /* Connection setup */

  /* When object is instantiated, you need select either setServerMode or
   * setClientMode */

  /* When in server mode, starts listening connections. When connected
   * by remote client, forwards all data there. In case of connection lost,
   * starts listening again. */
  bool setServerMode();

  /* When in client mode, starts connecting to remote. When connected, forwards
   * all data there. In case of disconnect, start connecting again. Tries to
   * connect perioidic basis until connection is made. */

  bool setClientMode(std::string destinationTcpAddress = "127.0.0.1",
                     int portNumber = defaultTimeToPicTcpRemoteConnectionPort);

  /* Helper function. Blocks until connection is set up or threshold time has
   * been elapsed */
  void waitForConnection(int maxWaitSec = 10);

  /* Logging API */

  /* API for logging generic text */
  void logText(const std::string &logStr);

  /* API for logging event */
  void logEvent(const std::string &eventName, bool eventStart) {
    if (eventStart) {
      logEventStart(eventName);
    } else {
      logEventStop(eventName);
    }
  }

  /* API for logging event start */
  void logEventStart(const std::string &eventName) {
    if (eventName.empty() == false) {
      logText(eventStartPrefixStr + eventName);
    }
  }

  /* API for logging event stop */
  void logEventStop(const std::string &eventName) {
    if (eventName.empty() == false) {
      logText(eventStopPrefixStr + eventName);
    }
  }

  /* API for logging event value */
  void logValue(const std::string &valueName, double value, int precision = 0) {
    if (valueName.empty() == false) {

      // Add double to stream
      // Create an output std::string stream
      std::stringstream absValueStreamConvertor;
      // Set Fixed -Point Notation
      absValueStreamConvertor << std::fixed;

      // Set precision to 2 digits
      absValueStreamConvertor << std::setprecision(precision);
      absValueStreamConvertor << value;

      logText(valueAbsPrefixStr + absValueStreamConvertor.str() + ";" +
              valueName);

      absValueStreamConvertor.clear();
    }
  }

  /* API for logging state machine */
  void logState(const std::string &&stateMachineName,
                const std::string &&machineNextState) {
    if (stateMachineName.empty() == false &&
        machineNextState.empty() == false) {
      logText(stateMachinePrefixStr + machineNextState + ";" +
              stateMachineName);
    }
  }

  /* API for logging messages between entities */
  void logMessage(const std::string &&senderName,
                  const std::string &&receiverName,
                  const std::string &&messageName) {
    if (senderName.empty() == false && receiverName.empty() == false &&
        messageName.empty() == false) {
      logText(messagePrefixStr + senderName + ";" + receiverName + ";" +
              messageName);
    }
  }

private:
  typedef chrono::duration<double> relativeTimeStampType;

  enum loggerConnectionMode {
    tcpClient,
    tcpServer
  } mConnectionMode = tcpServer;

  /* variables needed for server connection */
  enum handleValues { invalidHandleValue = -1 };

  chrono::system_clock::time_point mInstanceStartTime =
      chrono::system_clock::now();
  const std::string eventStartPrefixStr = "event;start;";
  const std::string eventStopPrefixStr = "event;stop;";
  const std::string valueAbsPrefixStr = "valueabs;";
  const std::string stateMachinePrefixStr = "state;";
  const std::string messagePrefixStr = "msg;";

  std::list<std::string> mLogLineTxList = {};
  std::mutex mLogLineTxListMutex = {};
  std::shared_ptr<thread> mSenderThread = {};
  bool mSenderThreadAbortRequested = false;
  bool mConnectionIsSetUp = false;

  int mSockfd = invalidHandleValue, mNewsockfd = invalidHandleValue;
  struct sockaddr_in mServerAddress = {};
  struct sockaddr_in mClientAddress = {};

  bool closeActiveConnection();
  bool sendToActiveConnection(const std::string &text);
  bool setupClientConnection(std::string address, int portNumber);
  bool setupServerListener();
  relativeTimeStampType getRelativeTimeStamp();
  std::string formatRelativeTimeStampSec(relativeTimeStampType timeStamp);
  void sendText(const std::string &text);
  void
  senderThreadProc(std::string destinationTcpAddress,
                   int portNumber); // can throw LoggerInternalsFailException
  void stopSenderThreadProc();
};

#endif /* TIMETOPICLOG_H_ */
